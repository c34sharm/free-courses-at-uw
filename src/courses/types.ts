
export interface Course {
    department: string; // e.g. "CS" or "ECE"
    courseNumber: number; // e.g. 101
    title: string; // e.g. "Intro to Computer Science"
    description: string; // e.g. "Learn how to code!"
    link: string; // e.g. "https://www.cs.ubc.ca"
    modes: "in-person" | "online" | "asynchronous" | "synchronous" | "hybrid" | "other";
    platform: string; // e.g. ["Canvas", "Piazza"]
    tags: string[]; // e.g. ["CPSC", "Software Engineering"]
    status: "Maintained" | "Deprecated" | "Discontinued but relevant";
    termsOffered: "Winter" | "Summer" | "Fall" | "Winter and Summer" | "Fall and Winter" | "Summer and Fall" | "All";

    // Optional fields
    credits?: number; // e.g. 3
    prereqs?: string[]; // e.g. ["CPSC 110", "CPSC 121"]
    coreqs?: string[]; // e.g. ["CPSC 210"]
    recommended?: string[]; // e.g. ["CPSC 213"]
    restrictions?: string[]; // e.g. ["Restricted to students in the BCS program"]
    notes?: string[]; // e.g. ["This course is hard"]
    equivalent?: string[]; // e.g. ["CPEN 221"]
}
