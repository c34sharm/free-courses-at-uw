import { Box, Chip, Link, Typography } from "@mui/material";
import React from "react";
import { Course } from "./types";
// is passed in as a prop
function CourseWidget(props: Course) {
  return (
    <div>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          maxWidth: "20vw",
          height: "20vh",
          p: 4,
          m: 2,
          borderRadius: 5,
          border: "1px solid #ffb1d8",
          overflow: "hidden",
        }}
      >
        <Typography
          variant="h5"
          component="div"
          sx={{ color: "#11111`" , pt: 2}}
          align="left"
          
        >
          {props.department} {props.courseNumber} {props.title}
        </Typography>
        <Typography
          variant="h6"
          component="div"
          sx={{
            color: "#ffb1d8",
            overflow: "hidden",
            textOverflow: "ellipsis",
            // whiteSpace: "nowrap",

            maxHeight: "6.5em",
          }}
          align="left"
        >
          {props.description}...
        </Typography>
        <Box
          sx={{
            display: "flex",
            justifyContent: "flex-end",
            width: "100%",
            my: 2,
          }}
        >
          <Typography
            variant="h6"
            component="div"
            sx={{
              color: "#111111",
              borderRadius: 5,
              border: "1px solid #ffb1d8",
              transition: "background 1s, color 1s",
              ":hover": {
                color: "#ffb1d8",
                transform: "scale3d(1.05, 1.05, 1)",
              },
            }}
          >
            <Link href={props.link} underline="hover" sx={{ color: "#ffb1d8" }}>
              <Chip label={props.platform} sx={{ color: "#ffb1d8" }} />
            </Link>
          </Typography>
        </Box>
      </Box>
    </div>
  );
}

export default CourseWidget;
