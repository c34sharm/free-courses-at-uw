import React from "react";
import "./App.css";
import CourseWidget from "./courses/CourseWidget";
import { courses } from "./courses/data";
import {
  AppBar,
  Box,
  Button,
  Grid,
  IconButton,
  Toolbar,
  Typography,
} from "@mui/material";

// const courses is a list of type Course


function App() {
  const navItems = ["Home", "Courses", "Add a course", "Contact"];
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <div className="App">
      <header className="App-header">
        {/* Website for fre */}
        <AppBar component="nav" color="primary" position="absolute"sx={{background: '#6F3B55'}}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              sx={{ mr: 2, display: { sm: "none" } }}
            ></IconButton>
            <Typography
              variant="h6"
              component="div"
              sx={{ flexGrow: 1, display: { xs: "none", sm: "block" } }}
            >
              Free Course Contents available from the University of Waterloo
            </Typography>
            <Box sx={{ display: { xs: "none", sm: "block" } }}>
              {navItems.map((item) => (
                <Button key={item} sx={{ color: "#fff" }}>
                  {item}
                </Button>
              ))}
            </Box>
          </Toolbar>
        </AppBar>
        <Grid sx={{
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'flex-start',
          alignContent: 'flex-start',
          py: 8,
          m: 4,
        }}>
        {courses.map((course) => (
          <CourseWidget {...course} />
          ))}
          </Grid>
      </header>
    </div>
  );
}

export default App;
